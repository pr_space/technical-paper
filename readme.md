# The Box Model

According to CSS, everything on a web page is a box,Literally a rectangle of pixels. The "box model" defines how the different parts of a box work together to create a box.

The parts of the model are here below:


<img src="https://www.washington.edu/accesscomputing/webd2/student/unit3/images/boxmodel.gif">

<a href="https://imgbb.com/"><img src="https://gitlab.com/pr_space/technical-paper/-/raw/master/images/1/The-box-model-Learn-web-development-MDN.png" alt="The-box-model-Learn-web-development-MDN" border="0"></a>


## Content box: 
The area where your content is displayed. Properties like width and height would help.


## Padding box: 
The padding sits around the content as white space; its size can be controlled using `padding` and related properties.

## Border box: 
The border box wraps the content and any padding. 

## Margin box: 
The margin is the outermost layer, wrapping the content, padding and border as whitespace between this box and other elements. Its size can be controlled using margin and related properties.


Also, There is a standard and an alternate box model. Most browsers use the standard box model. 


# Block and Inline elements 

In CSS, we broadly have two types of boxes — block boxes and inline boxes. These characteristics refer to how the box behaves in terms of page flow, and in relation to other boxes on the page:

<a href=""><img src="https://blog.4psa.com/wp-content/uploads/block-inline1.png" border="0"></a>


If a box is defined as a **block**, it will behave in the following ways:

<ul>
  <li>The box will break onto a new line.</li>
  <li>The box will extend in the inline direction to fill the space available in its container.</li>
  <li>The width and height properties are respected.
    Padding, margin and border will cause other elements to be pushed away from the box
    </li>
  <li>Unless we decide to change the display type to inline, elements such as headings (e.g. < h1 > and < p > all use block as their outer display type by default.
  </li>
</ul>

If a box has an outer display type of **inline**, then:

<ul>
  <li>The box will not break onto a new line.
    </li>
  <li>The width and height properties will not apply.</li>
  <li>Vertical padding, margins, and borders will apply but will not cause other inline boxes to move away from the box.</li>
  <li>Horizontal padding, margins, and borders will apply and will cause other inline boxes to move away from the box.</li>
  <li>The < a > element, used for links, < span >, < em > and < strong > are all examples of elements that will display inline by default.</li>
</ul>  


# Positioning - Absolute, relative, fixed and static  

Here, Lets alter the blue box's position to understand these concepts. So, when the blue box's property is set to:

**Case 1**

`position: static`


The element is positioned according to the normal flow of the document. The top, right, bottom, left, and z-index properties have no effect. This is the default value.

<a href="https://imgbb.com/"><img src="https://gitlab.com/pr_space/technical-paper/-/raw/master/images/3/static.png" alt="The-box-model-Learn-web-development-MDN" border="0"></a>

**Case 2**

`position: relative;`
`top: 40px; left: 40px;`

The element is positioned according to the normal flow of the document, and then offset relative to itself based on the values of top, right, bottom, and left. The offset does not affect the position of any other elements; thus, the space given for the element in the page layout is the same as if position were static.
This value creates a new stacking context when the value of z-index is not auto. Its effect on table-*-group, table-row, table-column, table-cell, and table-caption elements is undefined.

<a href="https://imgbb.com/"><img src="https://gitlab.com/pr_space/technical-paper/-/raw/master/images/3/relative.png" alt="The-box-model-Learn-web-development-MDN" border="0"></a>

**Case 3**

`position: absolute;`

 `top: 40px; ` ` left: 40px;`

The element is removed from the normal document flow, and no space is created for the element in the page layout. It is positioned relative to its closest positioned ancestor, if any; otherwise, it is placed relative to the initial containing block. Its final position is determined by the values of top, right, bottom, and left.
This value creates a new stacking context when the value of z-index is not auto. The margins of absolutely positioned boxes do not collapse with other margins.

<a href="https://imgbb.com/"><img src="https://gitlab.com/pr_space/technical-paper/-/raw/master/images/3/absolute.png" alt="The-box-model-Learn-web-development-MDN" border="0"></a>

**Case 4**

`position: fixed;`

 `top: 50px;`

The element is removed from the normal document flow, and no space is created for the element in the page layout. It is positioned relative to the initial containing block established by the viewport, except when one of its ancestors has a transform, perspective, or filter property set to something other than none (see the CSS Transforms Spec), in which case that ancestor behaves as the containing block. (Note that there are browser inconsistencies with perspective and filter contributing to containing block formation.) Its final position is determined by the values of top, right, bottom, and left.

<a href="https://imgbb.com/"><img src="https://gitlab.com/pr_space/technical-paper/-/raw/master/images/3/fixed.png" alt="The-box-model-Learn-web-development-MDN" border="0"></a>


# References

<ol>
    <li><a href="https://www.washington.edu/accesscomputing/webd2/student/unit3/module4/lesson1.html">Washington Edu article on Box Model</a></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/The_box_model">MDN Documentation</a></li>
    <li><a href="https://blog.4psa.com/wp-content/uploads/block-inline1.png">Picture for Block and Inline section</a></li>
</ol>